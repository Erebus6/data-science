# Data Science

This repository contains the code demonstrated in the YouTube tutorial series Data Science with R 

## Introduction to Data Science

[Data Science with R - What is Data Science](https://www.youtube.com/watch?v=2Uga73o30Rs)

[Data Science with R - Data Science Analysis Techniques](https://www.youtube.com/watch?v=BBECgqGvy3g)

[Data Science with R - Data Types in R](https://www.youtube.com/watch?v=seJJbnQ-jIg)

[Data Science with R - Variables in R](https://www.youtube.com/watch?v=2nNjvjeAO4s)

[Data Science with R - Operators in R](https://www.youtube.com/watch?v=xOQxeGu0-V0)

[Data Science with R – Conditionals and loops in R](https://www.youtube.com/watch?v=4qakqHvFMSU)

## Introduction to Probability

[Mathematics for Data Science – Set Theory](https://www.youtube.com/watch?v=QLvgFZeB_Bk)

## Repozitories

[GitHub](https://github.com/rebus90/DataScience)

[GitLab](https://gitlab.com/joalduk/data-science/)

[GitBucket](https://gitbucket.herokuapp.com/zoraniv/DataScience)

[NotABug](https://notabug.org/zoraniv/DataScience/wiki/Data+Science)

[CodeBerg](https://codeberg.org/zoraniv/DataScience/wiki/Data-Science)

[DevAzure](https://dev.azure.com/zoki349/_git/Data%20Science)

[BackLog](https://datasciece.backlog.com/wiki/DATASCIENCE/Home)

[GitGud](https://gitgud.io/zoraniv/data-science/)